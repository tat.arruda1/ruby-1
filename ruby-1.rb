arrayPrincipal = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]

def soma_arrays(array)
    soma = 0
    array.each do |item| #iterar por cada array
      for i in item #iterar por cada item de cada array
        #puts i
        soma += i #soma os elementos de cada array
      end 
    end
  puts soma
end

soma_arrays(arrayPrincipal)

def multiplica_arrays(array)
    contador = 1
  array.each do |item| #iterar por cada array
      for i in item #iterar por cada item de cada array
          contador *= i #multiplica os elementos de cada array
      end
  end
  puts contador
end

multiplica_arrays(arrayPrincipal)


hash_principal = {:chave1 => 5, :chave2 => 30, :chave3 => 20}

def pegaValor(hash)
    novaArray = [] #cria array vazia
    hash.each do |chave, valor| #itera pela hash
      #puts valor
      novaArray << valor ** 2 #adiciona na nova array os valores elevados ao quadrado
    end
    print novaArray #printa array
end

pegaValor(hash_principal)
            
array_numeros = [25, 35, 45]
def retorna_array_string(array)
    novarray = []
    array.each do |number|
      novarray << number.to_s #retorna cada item da array em formato de string e add na nova array
    end
    print novarray
  end 
  
retorna_array_string(array_numeros)
  

entrada = [3, 6, 7, 8]
def retorna_array_divisiveis(array)
    novarray = []
    array.each do |number|
      if (number % 3 == 0) #checa se cada elemento da array é divisivel por 3. se for, insere na nova array
        novarray << number
      end
    end
    print novarray
  end 

  retorna_array_divisiveis(entrada)

#Crie uma função que dado dois valores, retorne a divisão deles (a divisão deve retornar um float).
#Ex: Entrada: 5, 2
    #Saída: 2.5

def retorna_divisao(a, b)

  a_float = a.to_f
  b_float = b.to_f
  divisao = (a_float / b_float)
  return divisao

end

puts retorna_divisao(5, 2)


    




