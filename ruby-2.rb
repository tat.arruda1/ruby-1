class Usuario
    attr_accessor :email, :senha, :nome, :logado, :nascimento   #leitura e escrita desse att no objeto 
    @@quantidade_de_usuarios = 0

    def initialize(email:, senha:, nome:)
        @email = email
        @senha = senha
        @nome = nome
        @nascimento = ano_nascimento
        @logado = false
        
        @@quantidade_de_usuarios += 1
    end

    def idade 
        ano_atual = 2020
        idade = ano_tual - @nascimento
        puts "A idade do usuario é " + idade
    end

    def logar(senha)

        if (senha == @senha)
            @loado = true
        puts "você está logado!"
        end

    end
    
    def deslogar 
        @loado = false
        puts "você está deslogado"
    end

end

class Aluno
    attr_accessor :turmas, :matricula, :periodo_letivo, :curso

    @@quantidade_de_alunos = 0
    def initialize(matricula:, periodo_letivo:, curso:)
        @turmas = []
        @matricula = matricula
        @periodo_letivo = periodo
        @curso = curso  

        @@quantidade_de_alunos += 1

    end

    def inscrever(nome_da_turma)
        @turmas << nome_da_turma
        puts "você está inscrito!"
    end

end

class Professor
    attr_accessor :matricula, :salario, :materias

    @@quantidade_de_professores = 0
    def initialize(matricula:, materias:)
        @matricula = matricula
        @salario = salario
        @materias = []

        @@quantidade_de_professores += 1

    end

    def adicionar_materia(nome_materia)
        @materias << nome_materia
        puts "matéria adicionada!"
    end

end

class Materia
    attr_accessor :ementa, :nome, :professores

    @@quantidade_de_materias = 0
    def initialize(ementa:, nome:)
        @ementa = ementa
        @nome = nome
        @professores = []

        @@quantidade_de_materias += 1
    end

    def adicionar_professor(nome_professor)
        @professores << nome_professor
        puts "professor adicionado!"
    end
end

class Turma
    attr_accessor :nome, :horario, :dias_da_semana, :inscriçao_aberta, :alunos

    @@quantidade_de_turmas = 0

    def initialize(nome:, horario:, dias_da_semana:, inscriçao_aberta:)
        @nome = nome
        @horario = horario
        @dias_da_semana = dias
        @inscriçao_aberta = false
        @alunos = []

        @@quantidade_de_turmas += 1
    end

    def abrir_inscricao
        @inscriçao_aberta = true
        puts "inscrições abertas!"
    end

    def fechar_inscricao
        @inscriçao_aberta = false
        puts "inscrições encerradas."
    end

    def adicionar_aluno(nome_aluno)
        @alunos << nome_aluno
        puts "aluno adicionado!"
    end

end